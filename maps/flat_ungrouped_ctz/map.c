#include <stdlib.h>
#include <string.h>
#include <pmmintrin.h>
#include <emmintrin.h>

#include "../map.h"
#include "../hash.h"

typedef struct Pair Pair;
struct Pair {
    char* key;
    i32 value;
};

typedef struct Find_Result Find_Result;
struct Find_Result {
    Pair* pair; 
    usize empty_index;
};

typedef struct Map Map;
struct Map {
    usize size;
    usize cap;
    usize tombs;
    Pair* data;
    u8* ctrl;
};

#define TOMB (0x7f)

static u64 H1(u64 h) {
    return h >> 7;
}

static u8 make_ctrl(u64 h) {
    return (h & 0x7f) | 0x80;
}

static Find_Result find(Map* m, char* key, u64 h) {
    const usize mod_mask = m->cap - 1;
    usize index = H1(h) & mod_mask;
    usize step = 17;

    const __m128i ctrl_mask = _mm_set1_epi8(make_ctrl(h));
    const __m128i zero = _mm_set1_epi8(0);

    while (1) {
        __builtin_prefetch(m->data + index);
        int empty_slots;
        int found_matches;
        {
            __m128i ctrl = _mm_loadu_si128((void*)(m->ctrl + index));
            empty_slots = _mm_movemask_epi8(_mm_cmpeq_epi8(zero, ctrl));
            found_matches = _mm_movemask_epi8(_mm_cmpeq_epi8(ctrl, ctrl_mask));
        }
        {
            usize real_index = index;
            while (found_matches) {
                usize ctz = __builtin_ctz(found_matches);
                real_index += ctz;
                real_index &= mod_mask;
                if (strcmp(m->data[real_index].key, key) == 0) {
                    return (Find_Result) { .pair = &m->data[real_index] };
                }
                found_matches >>= ctz + 1;
                real_index += 1;
            }
        }
        if (empty_slots) {
            return (Find_Result) { .pair = NULL, .empty_index = (index + __builtin_ctz(empty_slots)) & mod_mask};
        }
        index += step;
        step *= step;
        index &= mod_mask;
    }
}

static usize find_empty(Map* m, u64 h) {
    const usize mod_mask = m->cap - 1;
    usize index = H1(h) & mod_mask;
    const __m128i zero = _mm_set1_epi8(0);
    usize step = 17;

    while (1) {
        __builtin_prefetch(m->data + index);
        int found_matches;
        {
            __m128i ctrl = _mm_loadu_si128((void*)(m->ctrl + index));
            found_matches = _mm_movemask_epi8(_mm_cmpeq_epi8(zero, ctrl));
        }
        if (found_matches) {
            return (index + __builtin_ctz(found_matches)) & mod_mask;
        }
        index += step;
        step *= step;
        index &= mod_mask;
    }
}

Map* map_create(void) {
    Map* m = calloc(1, sizeof(*m));
    if (!m) return NULL;

    m->cap = 16;
    m->ctrl = calloc(1, 16 + m->cap * (sizeof(*m->data) + sizeof(*m->ctrl)));
    if (!m->ctrl) {
        free(m);
        return NULL;
    }
    m->data = (void*)(m->ctrl + m->cap + 16);
    return m;
}

void map_free(Map* m) {
    if (!m) return;
    free(m->ctrl);
    free(m);
}

int rehash(Map* m, usize new_cap) {
    Map new = { 0 };
    new.cap = new_cap;
    new.size = m->size;
    new.ctrl = calloc(1, 16 + new.cap * (sizeof(*new.data) + sizeof(*new.ctrl)));
    if (!new.ctrl) return -1;
    new.data = (void*)(new.ctrl + 16 + new.cap);

    for (usize i = 0; i < m->cap; i += 16) {
        __builtin_prefetch(m->data + i);
        u32 found_matches = _mm_movemask_epi8(_mm_load_si128((void*)(m->ctrl + i)));
        usize j = i;
        while (found_matches) {
            usize ctz = __builtin_ctz(found_matches);    
            found_matches >>= ctz;
            j += ctz;
            usize num_ones = __builtin_ctz(~found_matches); // number of ones
            for (usize k = 0; k < num_ones; ++k, ++j) {
                usize old_index = j;

                const u64 h    = hash(m->data[old_index].key);
                const u8  ctrl = make_ctrl(h);
                const usize new_index = find_empty(&new, h);
                new.ctrl[new_index] = ctrl;
                if (new_index < 16) new.ctrl[new_index + new.cap] = ctrl;
                new.data[new_index] = m->data[old_index];
            }
            found_matches >>= num_ones;
        }
    }

    free(m->ctrl);
    *m = new;
    return 0;
}

i32* map_get_or_insert(Map* m, char* key, i32 value) {
    u64 h = hash(key);
    usize empty_index;
    Pair* p;
    {
        Find_Result fr = find(m, key, h);
        empty_index = fr.empty_index;
        p = fr.pair;
    }
    if (!p) {
        if (m->size >= .75 * m->cap) {
            int err = rehash(m, m->cap << 1);
            if (err) return NULL;
            empty_index = find_empty(m, h);
        }
        m->size += 1;
        m->ctrl[empty_index] = make_ctrl(h);
        if (empty_index < 16) m->ctrl[empty_index + m->cap] = make_ctrl(h);
        m->data[empty_index] = (Pair) { .key = key, .value = value };
        p = &m->data[empty_index];
    }
    return &p->value;
}

i32* map_insert(Map* m, char* key, i32 value) {
    i32* v = map_get_or_insert(m, key, value);
    if (!v) return NULL;
    *v = value;
    return v;
}

i32* map_get(Map* m, char* key) {
    Find_Result fr = find(m, key, hash(key));
    return fr.pair ? &fr.pair->value : NULL;
}

bool map_contains(Map* m, char* key) {
    return map_get(m, key) != NULL;
}

usize map_size(Map* m) {
    return m->size;
}

int map_remove(Map* m, char* key, char** dest_key, i32* dest_value) {
    Pair* p;
    {
        Find_Result fr = find(m, key, hash(key));
        p = fr.pair;
    }
    if (!p) return -1;

    if (dest_key) {
        *dest_key = p->key;
    }
    if (dest_value) {
        *dest_value = p->value;
    }

    usize real_index = p - m->data;
    m->tombs += 1;

    m->ctrl[real_index] = TOMB;
    if (real_index < 16) m->ctrl[real_index + m->cap] = TOMB;

    if (m->tombs >= .1 * m->cap) {
        int err = rehash(m, m->cap);
        if (err) return 1;
    }
    return 0;
}

void map_foreach(Map* m, Map_Iterator_Function f, void* arg) {
    for (usize i = 0; i < m->cap; i += 16) {
        __builtin_prefetch(m->data + i);
        u32 found_matches = _mm_movemask_epi8(_mm_load_si128((void*)(m->ctrl + i)));
        usize j = i;
        while (found_matches) {
            usize ctz = __builtin_ctz(found_matches);
            found_matches >>= ctz;
            j += ctz;
            usize num_ones = __builtin_ctz(~found_matches);
            found_matches >>= num_ones;
            for (; num_ones; ++j, --num_ones) {
                usize real_index = j;
                f(&m->data[real_index].key, &m->data[real_index].value, arg);
            }
        }
    }
}
