#include <stdlib.h>
#include <string.h>

#include "../map.h"
#include "../hash.h"

typedef struct Node Node;
struct Node {
    Node* child[4];
    char* key;
    i32 value;
};

typedef struct Map Map;
struct Map {
    Node* root;
    Node* unused;
    usize size;
};

Map* map_create(void) {
    return calloc(1, sizeof(Map));
}

static void node_free(Node** n) {
    if (n == NULL) return;
    if (*n == NULL) return;
    node_free(&(*n)->child[0]);
    node_free(&(*n)->child[1]);
    node_free(&(*n)->child[2]);
    node_free(&(*n)->child[3]);

    free(*n);
    *n = NULL;
}

void map_free(Map* m) {
    if (m == NULL) return;
    node_free(&m->root);
    node_free(&m->unused);
    free(m);
}

static Node* node_create(Map* m, char* key, i32 value) {
    Node* n;
    if (m->unused != NULL) {
        n = m->unused;
        m->unused = m->unused->child[0];
    } else {
        n = malloc(sizeof(*n));
        if (n == NULL) return NULL;
        m->size += 1;
    }
    *n = (Node) {
        .key = key,
        .value = value,
    };
    return n;
}

static void node_destroy(Map* m, Node** n) {
    if (*n == NULL) return;

    node_destroy(m, &(*n)->child[0]);
    node_destroy(m, &(*n)->child[1]);
    node_destroy(m, &(*n)->child[2]);
    node_destroy(m, &(*n)->child[3]);

    memset(*n, 0, sizeof(**n));
    (*n)->child[0] = m->unused;
    m->unused = *n;
    *n = NULL;
}

static Node** lookup_(Node** n, char* key, u64 h) {
    if (*n == NULL) return n;
    if (strcmp(key, (*n)->key) == 0) return n;
    return lookup_(&(*n)->child[h & 3], key, h >> 2);
}

static Node** lookup(Map* m, char* key) {
    return lookup_(&m->root, key, hash(key));
}

i32* map_get_or_insert(Map* m, char* key, i32 value) {
    Node** n = lookup(m, key);
    if (*n == NULL) {
        *n = node_create(m, key, value);
    }
    return *n != NULL ? &(*n)->value : NULL;
}

i32* map_insert(Map* m, char* key, i32 value) {
    i32* value_dest = map_get_or_insert(m, key, value);
    if (value_dest == NULL) return NULL;
    *value_dest = value;
    return value_dest;
}

i32* map_get(Map* m, char* key) {
    Node* n = *lookup(m, key);
    return n != NULL ? &n->value : NULL;
}

bool map_contains(Map* m, char* key) {
    return *lookup(m, key) != NULL;
}

int map_remove(Map* m, char* key, char** dest_key, i32* dest_value) {
    Node** np = lookup(m, key);
    if (*np == NULL) return 1;

    Node* n = *np;
    *np = NULL;

    if (dest_key) {
        *dest_key = n->key;
    }
    if (dest_value) {
        *dest_value = n->value;
    }
    if (n->child[0] != NULL) *lookup(m, n->child[0]->key) = n->child[0];
    if (n->child[1] != NULL) *lookup(m, n->child[1]->key) = n->child[1];
    if (n->child[2] != NULL) *lookup(m, n->child[2]->key) = n->child[2];
    if (n->child[3] != NULL) *lookup(m, n->child[3]->key) = n->child[3];
    node_destroy(m, &n);
    return 0;
}

void map_cleart(Map* m) {
    m->size = 0;
    node_destroy(m, &m->root);
}

int map_reserve(Map* m, usize n) {
    for (usize i = 0; i < n; ++i) {
        Node* n = calloc(1, sizeof(*n));
        if (n == NULL) return -1;
        n->child[0] = m->unused;
        m->unused = n;
    }
    return 0;
}

usize map_size(Map* m) {
    return m->size;
}

static void node_foreach(Node* n, Map_Iterator_Function f, void* arg) {
    if (!n) return;
    f(&n->key, &n->value, arg);
    node_foreach(n->child[0], f, arg);
    node_foreach(n->child[1], f, arg);
    node_foreach(n->child[2], f, arg);
    node_foreach(n->child[3], f, arg);
}

void map_foreach(Map* m, Map_Iterator_Function f, void* arg) {
    node_foreach(m->root, f, arg);
}
