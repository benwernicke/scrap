#include "map.h"
#include <stdlib.h>
#include <holy_types.h>
#include <stdio.h>
#include <ctype.h>

#define PANIC(...) \
    do { \
        fprintf(stderr, "Error: "); \
        fprintf(stderr, __VA_ARGS__); \
        putc('\n', stderr); \
        exit(1); \
    } while (0)

#define PANIC_IF(b, ...) \
    do { \
        if (b) { \
            PANIC(__VA_ARGS__); \
        } \
    } while (0)

void print_pair(char** s, i32* v, void* arg) {
    (void)arg;
    printf("%s:\t%d\n", *s, *v);
}


int main(int argc, char** argv) {
    PANIC_IF(argc != 2, "wrong number of arguments");

    Map* m = map_create();

    // load entire FIle
    char* file_content = NULL;
    {
        FILE* f = fopen(argv[1], "r");
        PANIC_IF(!f, "could not open file: '%s'", argv[1]);

        fseek(f, 0, SEEK_END);
        usize size = ftell(f);
        rewind(f);
        file_content = calloc(1, size + 3);
        fread(file_content, 1, size, f);

        fclose(f);
    }

    // make histogram
    {
        char* s = file_content;
        for (; isspace(*s); ++s) { }
        while (*s) {
            char* start = s;
            for (; *s && !isspace(*s); ++s) { }
            *s = '\0';
            *map_get_or_insert(m, start, 0) += 1;
            s += 1;
        }
    }

    map_foreach(m, print_pair, NULL);

    map_free(m);

    free(file_content);

    return 0;
}
