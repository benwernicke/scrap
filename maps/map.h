#ifndef MAP_H
#define MAP_H

#include <belt/holy.h>
#include <stdbool.h>

typedef struct Map Map;
typedef void (*Map_Iterator_Function)(char** key, i32* value, void* arg);

// key is always char*
// val is always i32
// hash value is always u64

Map* map_create(void);  
void map_free(Map*);

i32*  map_insert(Map*, char*, i32);
i32*  map_get_or_insert(Map*, char*, i32);
i32*  map_get(Map*, char*);
char** map_get_key(Map*, char*);
bool  map_contains(Map*, char*);
int   map_remove(Map*, char*, char**, i32*);

void  map_clear(Map*);
int   map_reserve(Map*, usize);
usize map_size(Map*);

void map_foreach(Map*, Map_Iterator_Function, void* arg);

#endif // MAP_H
