#include "hash.h"
u64 hash(char* s)
{
    u64 h = 0x100;
    for (; *s; ++s) {
        h ^= *s;
        h *= 1111111111111111111u;
    }
    return h;
}
