#include <stdlib.h>
#include <string.h>

#include "../map.h"
#include "../hash.h"

typedef struct Element Element;
struct Element {
    char* key;
    i32 value;
};

typedef struct Map Map;
struct Map {
    Element* table;
    usize size;
    usize cap;
    usize tombs;
};

static bool is_empty(Element* e) {
    return !e->key;
}

static bool is_tomb(Element* e) {
    return (usize)e->key == (usize)1;
}

static bool is_valid(Element* e) {
    return !is_tomb(e) && !is_empty(e);
}

static Element* lookup(Map* m, char* key) {
    Element* i = m->table + (hash(key) & (m->cap - 1));
    Element* const end = m->table + m->cap;
    for (; i != end; ++i) {
        if (is_empty(i)) return i;
        if (is_tomb(i)) continue;
        if (strcmp(i->key, key) == 0) return i;
    }
    i = m->table;
    for (;; ++i) {
        if (is_empty(i)) return i;
        if (is_tomb(i)) continue;
        if (strcmp(i->key, key) == 0) return i;
    }
}

static int rehash(Map* m, usize new_cap) {
    Map old = *m;

    m->cap = new_cap;
    m->table = calloc(m->cap, sizeof(*m->table));
    if (!m->table) {
       *m = old; 
       return -1;
    }

    Element* i = old.table;
    Element* const end = old.table + old.cap;
    for (; i != end; ++i) {
        if (is_valid(i)) {
            *lookup(m, i->key) = *i;
        }
    }

    m->tombs = 0;
    free(old.table);
    return 0;
}

Map* map_create(void) {
    Map* m = calloc(1, sizeof(*m));
    m->cap = 16;
    m->table = calloc(m->cap, sizeof(*m->table));
    if (!m->table) {
        free(m);
        return NULL;
    }
    return m;
}

void map_free(Map* m) {
    if (!m) return;
    free(m->table);
    free(m);
}

i32* map_get_or_insert(Map* m, char* key, i32 value) {
    Element* n = lookup(m, key);
    if (is_empty(n)) {
        if (m->size >= .75 * m->cap) {
            int err = rehash(m, m->cap << 1);
            if (err) return NULL;
            n = lookup(m, key);
        }
        m->size += 1;
        n->key = key;
        n->value = value;
    }
    return &n->value;
}

i32* map_insert(Map* m, char* key, i32 value) {
    i32* v = map_get_or_insert(m, key, value);
    if (!v) return NULL;
    *v = value;
    return v;
}

i32* map_get(Map* m, char* key) {
    Element* n = lookup(m, key);
    return is_empty(n) ? NULL : &n->value;
}

bool map_contains(Map* m, char* key) {
    return !is_empty(lookup(m, key));
}

usize map_size(Map* m) {
    return m->size;
}

void map_foreach(Map* m, Map_Iterator_Function f, void* arg) {
    Element* i = m->table;
    Element* const end = m->table + m->cap;
    for (; i != end; ++i) {
        if (is_valid(i)) {
            f(&i->key, &i->value, arg);
        }
    }
}

int map_remove(Map* m, char* key, char** dest_key, i32* dest_value) {
    Element* n = lookup(m, key);
    if (is_empty(n)) return -1;

    if (dest_key) {
        *dest_key = n->key;
    }
    if (dest_value) {
        *dest_value = n->value;
    }

    n->key = (void*)1;
    m->tombs += 1;
    m->size -= 1;

    if (m->tombs >= 0.1 * m->cap) {
        return rehash(m, m->cap);
    }
    return 0;
}
