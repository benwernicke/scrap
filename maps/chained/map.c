#include <string.h>
#include <stdlib.h>

#include "../map.h"
#include "../hash.h"

typedef struct Node Node;
struct Node {
    char* key;
    Node* next;
    i32 value;
};

typedef struct Map Map;
struct Map {
    Node** table;
    Node* unused;
    usize size;
    usize cap;
};

static Node** lookup(Map* m, char* key) {
    Node** iter = m->table + (hash(key) & (m->cap - 1)); 
    while (1) {
        if (!*iter) return iter;
        if (strcmp((*iter)->key, key) == 0) return iter;
        iter = &(*iter)->next;
    }
}

static int grow(Map* m) {
    usize  old_cap   = m->cap;
    Node** old_table = m->table;

    m->cap <<= 1;
    m->table = calloc(m->cap, sizeof(*m->table));
    if (!m->table) {
        m->table = old_table;
        m->cap   = old_cap;
        return -1;
    }

    for (Node** a = old_table; a != old_table + old_cap; ++a) {
        Node* b = *a;
        while (b) {
            Node* t = b;
            b = b->next;
            t->next = NULL;
            *lookup(m, t->key) = t;
        }
    }
    free(old_table);
    return 0;
}

Map* map_create(void) {
    Map* m = calloc(1, sizeof(*m));
    if (!m) return NULL;
    m->cap = 16;
    m->table = calloc(m->cap, sizeof(*m->table));
    if (!m->table) {
        free(m);
        return NULL;
    }
    return m;
}

void node_free(Node* n) {
    if (!n) return;
    node_free(n->next);
    free(n);
}

void map_free(Map* m) {
    if (!m) return; 

    for (Node** i = m->table; i != m->table + m->cap; ++i) {
        node_free(*i);
    }
    node_free(m->unused);
    free(m->table);
    free(m);
}

i32* map_get_or_insert(Map* m, char* key, i32 value) {
    Node** n = lookup(m, key);
    if (!*n) {
        if (m->size >= 0.75f * m->cap) {
            int err = grow(m);
            if (err) return NULL;
            n = lookup(m, key);
        }
        if (m->unused) {
            *n = m->unused;
            m->unused = m->unused->next;
        } else {
            *n = calloc(1, sizeof(**n));
        }
        memset(*n, 0, sizeof(**n));
        (*n)->key = key;
        (*n)->value = value;
        m->size += 1;
    }
    return &(*n)->value;
}

i32* map_insert(Map* m, char* key, i32 value) {
    i32* dest_value = map_get_or_insert(m, key, value);
    if (dest_value) {
        *dest_value = value;
    }
    return dest_value;
}

i32* map_get(Map* m, char* key) {
    Node* n = *lookup(m, key);
    return n ? &n->value : NULL;
}

bool map_contains(Map* m, char* key) {
    return *lookup(m, key) != NULL;
}

int map_remove(Map* m, char* key, char** dest_key, i32* dest_value) {
    Node** n = lookup(m, key);
    if (!*n) return -1;
    if (dest_key) {
        *dest_key = (*n)->key;
    }
    if (dest_value) {
        *dest_value = (*n)->value;
    }

    Node* t = *n;
    *n = (*n)->next;
    t->next = m->unused;
    m->unused = t;
    m->size -= 1;
    return 0;
}

static void node_clear(Map* m, Node* n) {
    if (!n) return;
    node_clear(m, n->next);
    n->next = m->unused;
    m->unused = n;
}

void map_clear(Map* m) {
    for (Node** iter = m->table; iter != m->table + m->cap; ++iter) {
        node_clear(m, *iter);
    }
    m->size = 0;
}

int map_reserve(Map* m, usize n) {
    (void) m;
    (void) n;
    return 1;
}

usize map_size(Map* m) {
    return m->size;
}

void map_foreach(Map* m, Map_Iterator_Function f, void* arg) {
    Node** i = m->table;
    Node** end = m->table + m->cap;
    for (; i != end; ++i) {
        Node* j = *i;
        while (j) {
            f(&j->key, &j->value, arg);
            j = j->next;
        }
    }
}
