#include <stdlib.h>
#include <string.h>
#include <pmmintrin.h>
#include <emmintrin.h>

#include "../map.h"
#include "../hash.h"

typedef struct Pair Pair;
struct Pair {
    char* key;
    i32 value;
};

typedef struct Map Map;
struct Map {
    usize size;
    usize cap;
    usize tombs;
    Pair* data;
    u8* ctrl;
};

#define TOMB (0x7f)

static u64 H1(u64 h) {
    return h >> 7;
}

static u8 make_ctrl(u64 h) {
    return (h & 0x7f) | 0x80;
}

static Pair* find(Map* m, char* key, u64 h) {
    const usize mod_mask = m->cap - 1;
    usize index = H1(h) & mod_mask;

    const __m128i ctrl_mask = _mm_set1_epi8(make_ctrl(h));
    const __m128i zero = _mm_set1_epi8(0);

    while (1) {
        u32 has_empty_slot;
        u32 found_matches;
        {
            __m128i ctrl = _mm_loadu_si128((void*)(m->ctrl + index));
            has_empty_slot = _mm_movemask_epi8(_mm_cmpeq_epi8(zero, ctrl));
            found_matches = _mm_movemask_epi8(_mm_cmpeq_epi8(ctrl, ctrl_mask));
        }
        {
            usize j = 0;
            while (found_matches) {
                if (found_matches & 1) {
                    usize real_index = (index + j) & mod_mask;
                    if (strcmp(m->data[real_index].key, key) == 0) {
                        return &m->data[real_index]; 
                    }
                }
                found_matches >>= 1;
                j += 1;
            }
        }
        if (has_empty_slot) return NULL;
        index += 16;
        index &= mod_mask;
    }
}

static usize find_empty(Map* m, char* key, u64 h) {
    const usize mod_mask = m->cap - 1;
    usize index = H1(h) & mod_mask;
    const __m128i zero = _mm_set1_epi8(0);

    while (1) {
        u32 found_matches;
        {
            __m128i ctrl = _mm_loadu_si128((void*)(m->ctrl + index));
            found_matches = _mm_movemask_epi8(_mm_cmpeq_epi8(zero, ctrl));
        }
        {
            usize j = 0;
            while (found_matches) {
                if (found_matches & 1) return (index + j) & mod_mask;
                found_matches >>= 1;
                j += 1;
            }
        }
        index += 16;
        index &= mod_mask;
    }
}

Map* map_create(void) {
    Map* m = calloc(1, sizeof(*m));
    if (!m) return NULL;

    m->cap = 16;
    m->ctrl = calloc(1, 16 + m->cap * (sizeof(*m->data) + sizeof(*m->ctrl)));
    if (!m->ctrl) {
        free(m);
        return NULL;
    }
    m->data = (void*)(m->ctrl + m->cap + 16);
    return m;
}

void map_free(Map* m) {
    if (!m) return;
    free(m->ctrl);
    free(m);
}

int rehash(Map* m, usize new_cap) {
    Map new = { 0 };
    new.cap = new_cap;
    new.size = m->size;
    new.ctrl = calloc(1, 16 + new.cap * (sizeof(*new.data) + sizeof(*new.ctrl)));
    if (!new.ctrl) return -1;
    new.data = (void*)(new.ctrl + 16 + new.cap);

    for (usize i = 0; i < m->cap; i += 16) {
        u32 found_matches = _mm_movemask_epi8(_mm_load_si128((void*)(m->ctrl + i)));
        usize j = 0;
        while (found_matches) {
            if (found_matches & 1) {
                m->size += 1;
                usize m_real_index = i + j;
                u64 h = hash(m->data[m_real_index].key);
                usize new_real_index = find_empty(&new, m->data[m_real_index].key, h);
                new.ctrl[new_real_index] = make_ctrl(h);
                if (new_real_index < 16) new.ctrl[new_real_index + new.cap] = make_ctrl(h);
                new.data[new_real_index] = m->data[m_real_index];
            }
            j += 1;
            found_matches >>= 1;
        }
    }
    free(m->ctrl);
    *m = new;
    return 0;
}

i32* map_get_or_insert(Map* m, char* key, i32 value) {
    u64 h = hash(key);
    Pair* p = find(m, key, h);
    if (!p) {
        if (m->size >= .75 * m->cap) {
            int err = rehash(m, m->cap << 1);
            if (err) return NULL;
        }
        m->size += 1;
        usize real_index = find_empty(m, key, h);
        m->ctrl[real_index] = make_ctrl(h);
        if (real_index < 16) m->ctrl[real_index + m->cap] = make_ctrl(h);
        m->data[real_index] = (Pair) { .key = key, .value = value };
        p = &m->data[real_index];
    }
    return &p->value;
}

i32* map_insert(Map* m, char* key, i32 value) {
    i32* v = map_get_or_insert(m, key, value);
    if (!v) return NULL;
    *v = value;
    return v;
}

i32* map_get(Map* m, char* key) {
    Pair* p = find(m, key, hash(key));
    return p ? &p->value : NULL;
}

bool map_contains(Map* m, char* key) {
    return find(m, key, hash(key)) != NULL;
}

usize map_size(Map* m) {
    return m->size;
}

int map_remove(Map* m, char* key, char** dest_key, i32* dest_value) {
    Pair* p = find(m, key, hash(key));
    if (!p) return -1;

    if (dest_key) {
        *dest_key = p->key;
    }
    if (dest_value) {
        *dest_value = p->value;
    }

    usize real_index = p - m->data;
    m->tombs += 1;

    m->ctrl[real_index] = TOMB;
    if (real_index < 16) m->ctrl[real_index + m->cap] = TOMB;

    if (m->tombs >= .1 * m->cap) {
        int err = rehash(m, m->cap);
        if (err) return 1;
    }
    return 0;
}

void map_foreach(Map* m, Map_Iterator_Function f, void* arg) {
    for (usize i = 0; i < m->cap; i += 16) {
        u32 found = _mm_movemask_epi8(_mm_load_si128((void*)(m->ctrl + i)));
        usize j = 0;
        while (found) {
            if (found & 1) {
                usize real_index = i + j;
                f(&m->data[real_index].key, &m->data[real_index].value, arg);
            }
            j += 1;
            found >>= 1;
        }
    }
}
