#include <stdlib.h>
#include <string.h>

#include "../map.h"
#include "../hash.h"

typedef struct Node Node;
struct Node {
    union {
        Node* child[2];
        struct {
            Node* left;
            Node* right;
        };
        Node* next;
    };
    char* key;
    u64 h;
    i32 value;
};

typedef struct Map Map;
struct Map {
    Node* root;
    usize size;
    Node* unused;
};

Map* map_create(void) {
    return calloc(1, sizeof(Map));
}

static void node_free(Node* n) {
    if (!n) return;
    node_free(n->left);
    node_free(n->right);
    free(n);
}

void map_free(Map* m) {
    if (!m) return;
    node_free(m->root);
    node_free(m->unused);
    free(m);
}

static Node** lookup_(Node** n, char* key, u64 h) {
    if (!*n) return n;
    if ((*n)->h == h && strcmp((*n)->key, key) == 0) return n;

    int dir = (*n)->h < h;
    return lookup_(&(*n)->child[dir], key, h);
}

static Node** lookup(Map* m, char* key) {
    return lookup_(&m->root, key, hash(key));
}

i32* map_get_or_insert(Map* m, char* key, i32 value) {
    Node** n = lookup(m, key);
    if (!*n) {
        m->size += 1;
        if (m->unused) {
            *n = m->unused;
            m->unused = m->unused->next;
        } else {
            *n = malloc(sizeof(**n));
            if (!*n) return NULL;
        }
        memset(*n, 0, sizeof(**n));

        **n = (Node) {
            .key = key,
            .value = value,
            .h = hash(key),
        };
    }
    return &(*n)->value;
}

i32* map_insert(Map* m, char* key, i32 value) {
    i32* v = map_get_or_insert(m, key, value);
    if (!v) return NULL;
    *v = value;
    return v;
}

i32* map_get(Map* m, char* key) {
    Node* n = *lookup(m, key);
    return n ? &n->value : NULL;
}

bool map_contains(Map* m, char* key) {
    return *lookup(m, key) != NULL;
}

static void node_foreach(Node* n, Map_Iterator_Function f, void* arg) {
    if (!n) return;
    f(&n->key, &n->value, arg);
    node_foreach(n->left, f, arg);
    node_foreach(n->right, f, arg);
}

void map_foreach(Map* m, Map_Iterator_Function f, void* arg) {
    node_foreach(m->root, f, arg);
}

usize map_size(Map* m) {
    return m->size;
}

int map_remove(Map* m, char* key, char** dest_key, i32* dest_value) {
    Node** n = lookup(m, key);
    if (!*n) return -1;

    if (dest_key) {
        *dest_key = (*n)->key;
    }
    if (dest_value) {
        *dest_value = (*n)->value;
    }

    m->size -= 1;

    Node* t = *n;
    *n = NULL;

    *lookup(m, t->left->key) = t->left;
    *lookup(m, t->right->key) = t->right;

    memset(t, 0, sizeof(*t));
    t->next = m->unused;
    m->unused = t;
    return 0;
}
