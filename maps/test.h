#ifndef TEST_H
#define TEST_H

#include <belt/holy.h>
#include <stdio.h>
#include <stdlib.h>

static u64 test_passed = 0;
static u64 test_failed = 0;

#define test(a) \
    do { \
        printf("[%s:%d] '%s': ", __func__, __LINE__, #a); \
        if (a) { \
            test_passed += 1; \
            printf("\033[32mpassed\033[0m\n"); \
        } else { \
            test_failed += 1; \
            printf("\033[31mfailed\033[0m\n"); \
        } \
    } while (0) 

static void test_wrapup(void) {
    printf("[test] overall\n\t\033[32mpass:\t\033[0m%lu\n\t\033[31mfailed:\t\033[0m%lu\n", test_passed, test_failed);
    if (test_failed != 0) {
        exit(1);
    }
}

#endif // TEST_H
