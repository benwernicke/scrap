#include <stdlib.h>
#include <string.h>
#include <emmintrin.h>

#include "../map.h"
#include "../hash.h"

typedef struct Pair Pair;
struct Pair {
    char* key;
    i32 value;
};

typedef struct Empty_Pair_Ref Empty_Pair_Ref;
struct Empty_Pair_Ref {
    Pair* data;
    u8* ctrl;
};

typedef struct Map Map;
struct Map {
    usize size;
    usize cap;
    usize tombs;
    u8* ctrl;
    Pair* data;
};

static u64 H1(u64 seed, u64 h) {
    return (h >> 7) ^ seed;
}

static u8 make_ctrl(u64 h) {
    return (h & 0x7f) | 0x80;
}

static Pair* find(Map* m, char* key, u64 h) {
    u64 index = (H1((u64)m, h) & ((m->cap >> 4) - 1)) << 4;
    const u64 mod_mask = m->cap - 1;
    const __m128i zero = _mm_set1_epi8(0);
    const __m128i ctrl_mask = _mm_set1_epi8(make_ctrl(h));

    while (1) {
        u32 has_empty_slot;
        u32 matches;
        {
            __m128i ctrl   = _mm_load_si128((void*)(m->ctrl + index));
            has_empty_slot = _mm_movemask_epi8(_mm_cmpeq_epi8(zero, ctrl));
            matches        = _mm_movemask_epi8(_mm_cmpeq_epi8(ctrl_mask, ctrl));
        }
        {
            usize matches_index = 0;
            while (matches) {
                if (matches & 1) {
                    Pair* p = &m->data[index + matches_index];
                    if (strcmp(p->key, key) == 0) return p;
                }
                matches >>= 1;
                matches_index += 1;
            }
        }
        if (has_empty_slot) return NULL;

        index += 16;
        index &= mod_mask;
    }
}

static Empty_Pair_Ref find_empty(Map* m, char* key, u64 h, u64 seed) {
    u64 index = (H1(seed, h) & ((m->cap >> 4) - 1)) << 4;
    const u64 mod_mask = m->cap - 1;
    const __m128i zero = _mm_set1_epi8(0);

    while (1) {
        u32 matches;
        {
            __m128i ctrl   = _mm_load_si128((void*)(m->ctrl + index));
            matches = _mm_movemask_epi8(_mm_cmpeq_epi8(zero, ctrl));
        }
        {
            usize matches_index = 0;
            while (matches) {
                if (matches & 1) {
                    return (Empty_Pair_Ref) { 
                        .data = &m->data[index + matches_index], 
                        .ctrl = &m->ctrl[index + matches_index]
                    };
                }
                matches >>= 1;
                matches_index += 1;
            }
        }
        index += 16;
        index &= mod_mask;
    }
}

static int rehash(Map* m, usize new_cap) {
    Map new = { 0 };
    new.cap = new_cap;
    new.size = m->size;

    new.ctrl = calloc(1, new.cap * (sizeof(*new.ctrl) + sizeof(*new.data)));
    if (!new.ctrl) return -1;
    new.data = (void*)(new.ctrl + new.cap);

    /*for (usize i = 0; i < m->cap; ++i) {*/
        /*if (is_full(m->ctrl[i])) {*/
            /*u64 h = hash(m->data[i].key);*/
            /*Empty_Pair_Ref epr = find_empty(&new, m->data[i].key, h, (usize)m);*/
            /**epr.data = m->data[i];*/
            /**epr.ctrl = make_ctrl(h);*/
        /*}*/
    /*}*/

    for (usize i = 0; i < m->cap; i += 16) {
        u32 slot_mask = _mm_movemask_epi8(_mm_load_si128((void*)(m->ctrl + i)));
        usize j = 0;
        while (slot_mask) {
            if (slot_mask & 1) {
                const usize index = i + j;
                u64 h = hash(m->data[index].key);
                Empty_Pair_Ref epr = find_empty(&new, m->data[index].key, h, (usize)m);
                *epr.data = m->data[index];
                *epr.ctrl = make_ctrl(h);
            }
            j += 1;
            slot_mask >>= 1;
        }
    }

    free(m->ctrl);
    *m = new;
    return 0;
}

Map* map_create(void) {
    Map* m = calloc(1, sizeof(*m));
    if (!m) return NULL;
    m->cap = 16;
    m->ctrl = calloc(1, m->cap * (sizeof(*m->ctrl) + sizeof(*m->data)));
    if (!m->ctrl) {
        free(m);
        return NULL;
    }
    m->data = (void*)(m->ctrl + m->cap);
    return m;
}

void map_free(Map* m) {
    if (!m) return;
    free(m->ctrl);
    free(m);
}

i32* map_get_or_insert(Map* m, char* key, i32 value) {
    u64 h = hash(key);
    Pair* p = find(m, key, h);
    if (!p) {
        if (m->size >= m->cap * .75f) {
            int err = rehash(m, m->cap << 1);
            if (err) return NULL;
        }

        Empty_Pair_Ref r = find_empty(m, key, h, (usize)m);
        *r.ctrl = make_ctrl(h);
        *r.data =  (Pair) { .key = key, .value = value };

        m->size += 1;
        return &r.data->value;
    }
    return &p->value;
}

i32* map_insert(Map* m, char* key, i32 value) {
    i32* v = map_get_or_insert(m, key, value);
    if (!v) return NULL;
    *v = value;
    return v;
}

i32* map_get(Map* m, char* key) {
    Pair* p = find(m, key, hash(key));
    return p ? &p->value : NULL;
}

bool map_contains(Map* m, char* key) {
    return find(m, key, hash(key)) != NULL;
}

void map_foreach(Map* m, Map_Iterator_Function f, void* arg) {
    for (usize i = 0; i < m->cap; i += 16) {
        __m128i ctrl = _mm_load_si128((void*)(m->ctrl + i)); 
        u32 bitmask = _mm_movemask_epi8(ctrl);
        usize j = 0;
        while (bitmask) {
            if (bitmask & 1) {
                f(&m->data[i + j].key, &m->data[i + j].value, arg);
            }
            j += 1;
            bitmask >>= 1;
        }
    }
}

usize map_size(Map* m) {
    return m->size;
}

int map_remove(Map* m, char* key, char** dest_key, i32* dest_value) {
    Pair* p = find(m, key, hash(key));
    if (!p) return -1;

    if (dest_key) {
        *dest_key = p->key;
    }
    if (dest_value) {
        *dest_value = p->value;
    }

    m->ctrl[p - m->data] = 0x7f;
    m->size -= 1;
    m->tombs += 1;

    if (m->tombs >= m->cap * .1) {
        int err = rehash(m, m->cap);
        if (err) return 1;
    }
    return 0;
}
