#include <stdlib.h>
#include <string.h>

#include "../map.h"
#include "../hash.h"

typedef struct Node Node;
struct Node {
    Node* next;
    char* key;
    i32 value;
};

typedef struct Map Map;
struct Map {
    usize tombs;
    Node* buffer;
    usize size;
    usize cap;
    usize buffer_index;
    Node* unused;
    Node** table;
};

static usize buffer_cap(Map* m) {
    return m->cap * 0.75 + 1;
}

Map* map_create(void) {
    Map* m = calloc(1, sizeof(*m));
    if (!m) return NULL;
    m->cap = 16;
    m->table = calloc(1, m->cap * sizeof(*m->table));
    m->buffer = calloc(1, buffer_cap(m) * sizeof(*m->buffer));
    if (!m->table || !m->buffer) {
        free(m->table);
        free(m->buffer);
        free(m);
        return NULL;
    }
    return m;
}

void map_free(Map* m) {
    if (!m) return;
    free(m->table);
    free(m->buffer);
    free(m);
}

static Node** lookup(Map* m, char* key) {
    Node** i = m->table + (hash(key) & (m->cap - 1));

    while (1) {
        if (!*i) return i;
        if (strcmp((*i)->key, key) == 0) return i;
        i = &(*i)->next;
    }
}

static int grow(Map* m) {
    m->cap <<= 1;
    m->unused = NULL;

    m->table = realloc(m->table, sizeof(*m->table) * m->cap);
    memset(m->table, 0, sizeof(*m->table) * m->cap);

    m->buffer = realloc(m->buffer, sizeof(*m->buffer) * buffer_cap(m));

    if (!m->table || !m->buffer) return -1;

    for (Node* i = m->buffer; i != m->buffer + m->buffer_index; ++i) {
        if (!i->key) {
            i->next = m->unused;
            m->unused = i;
        } else {
            i->next = NULL;
            *lookup(m, i->key) = i;
        }
    }
    return 0;
}

i32* map_get_or_insert(Map* m, char* key, i32 value) {
    Node** n = lookup(m, key);
    if (!*n) {
        if (m->size >= m->cap * 0.75) {
            int err = grow(m);
            if (err) return NULL;
            n = lookup(m, key);
        }

        m->size += 1;

        if (m->unused) {
            *n = m->unused;
            m->unused = m->unused->next;
        } else {
            *n = &m->buffer[m->buffer_index++];
        }

        memset(*n, 0, sizeof(**n));

        **n = (Node) {
            .key = key,
            .value = value,
        };
    }
    return &(*n)->value;
}

i32* map_insert(Map* m, char* key, i32 value) {
    i32* v = map_get_or_insert(m, key, value);
    if (!v) return NULL;
    *v = value;
    return v;
}

i32* map_get(Map* m, char* key) {
    Node* n = *lookup(m, key);
    return n ? &n->value : NULL;
}

bool map_contains(Map* m, char* key) {
    return *lookup(m, key) != NULL;
}

usize map_size(Map* m) {
    return m->size;
}

void map_clear(Map* m) {
    m->size = 0;
    memset(m->table, 0, sizeof(*m->table) * m->cap);
    m->buffer_index = 0;
    m->unused = NULL;
}

int map_remove(Map* m, char* key, char** dest_key, i32* dest_value) {
    Node** n = lookup(m, key);
    if (!*n) return -1;
    if (dest_key) {
        *dest_key = (*n)->key;
    }
    if (dest_value) {
        *dest_value = (*n)->value;
    }

    Node* t = *n;
    *n = t->next;
    t->key = NULL;

    t->next = m->unused;
    m->unused = t;
    return 0;
}

void map_foreach(Map* m, Map_Iterator_Function f, void* arg) {
    Node* i = m->buffer;
    Node* end = m->buffer + m->buffer_index;
    for (; i != end; ++i) {
        if (i->key) {
            f(&i->key, &i->value, arg);
        }
    }
}
