#include <stdio.h>
#include <holy_types.h>
#include <stdarg.h>

typedef struct Vec Vec;
struct Vec {
    float x;
    float y;
    float z;
};

void print_vec_impl(Vec v) {
    printf("(%f, %f, %f)\n", v.x, v.y, v.z);
}

#define GLUE(A, B) A##B
#define print_vec_floats(a, b, c, ...) print_vec_impl((Vec) { .x = a, .y = b, .z = c} )
#define print_vec_vec(a, ...) print_vec_impl(a)
#define print_vec_(a, b, c, d, ...) GLUE(print_vec_, d)(a, b, c)
#define print_vec(...) print_vec_(__VA_ARGS__, floats, 2, vec)

int main(void) {
    Vec v0 = { 1, 2, 3};
    Vec v1 = { 69, 420, 666};
    print_vec(v0);
    print_vec(v1);
    print_vec(0, 0, 0);
    print_vec(69, 69, 69);
    return 0;
}
