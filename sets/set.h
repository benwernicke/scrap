#ifndef SET_H
#define SET_H

#include <holy_types.h>
#include <stdbool.h>

typedef struct Set Set;

Set* set_create(void);
void set_free(Set*);

bool set_contains(Set*, u64);
u64* set_get_key(Set*, u64);

u64* set_insert(Set*, u64);

void set_clear(Set*);
usize set_size(Set*);
int set_reserve(Set*, usize);

int set_remove(Set* s, u64, u64*);

#endif // SET_H
