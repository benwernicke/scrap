#include "set.h"

#include <stdlib.h>

typedef struct Set Set;
struct Set {
    u64* body;
    usize size;
    usize cap;
};

Set* set_create(void) {
    return calloc(1, sizeof(Set));
}

void set_free(Set* set) {
    if (!set) return;
    free(set->body);
    free(set);
}

u64* set_get_key(Set* set, u64 key) {
    u64* i = set->body;
    u64* const e = set->body + set->size;
    for (; i != e; ++i) {
        if (*i == key) {
            return i;
        }
    }
    return NULL;
}

bool set_contains(Set* set, u64 key) {
    return set_get_key(set, key) != NULL;
}

u64* set_insert(Set* set, u64 key) {
    u64* i = set_get(set, key);
    if (i) return i;

    if (set->size >= set->cap) {
        set->cap += 8;
        set->cap <<= 1;
        set->body = realloc(set->body, set->cap * sizeof(*set->body));
        if (!set->body) return NULL;
    }
    i = &set->body[set->size++];
    *i = key;
    return i;
}

void set_clear(Set* set) {
    set->size = 0;
}

int set_reserve(Set* set, usize new_cap) {
    if (new_cap < set->cap) return 0;
    set->cap = new_cap;
    set->body = realloc(set->body, set->cap * sizeof(*set->body));
    return set->body ? 0 : -1;
}

int set_remove(Set* set, u64 key) {
    u64* i = set_get(set, key);
    if (!i) return -1;
    memmove(i, i + 1, set->size - ((i + 1) - s->body));
    set->size -= 1;
    return 0;
}

usize set_size(Set* set) {
    return set->size;
}
