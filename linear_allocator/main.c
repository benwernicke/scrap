#include <stdlib.h>
#include <stdio.h>
#include <holy_types.h>

typedef u8 byte;

typedef struct Allocator Allocator;
struct Allocator {
    Allocator* next;
    usize used;
    usize alloced;
    u8 buffer[];
};

typedef struct Allocator_Interface Allocator_Interface;
struct Allocator_Interface {
    void*(*alloc)(usize);
    void(*discard)(void);
};

// how to 16 byte align
static void* allo_alloc(Allocator* a, usize n) {
    usize align = n & 15;
    if (align) {
        n += 16 - align;
    }
    if (a->used + a >= a->alloced) {
        usize new_alloced = a->alloced << 1 + n;
    }
}

static Allocator_Interface allocator = (Allocator) {
    .alloc = allo_alloc;
    .discard = allo_discard;
};
