	.file	"vec_from_memview.c"
	.text
	.p2align 4
	.globl	vec_from_memview
	.type	vec_from_memview, @function
vec_from_memview:
.LFB5:
	.cfi_startproc
	testl	%edx, %edx
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movq	%rdi, %rax
	je	.L6
	movl	%edx, %ebx
	movl	%edx, %edx
	leaq	8(,%rdx,4), %rdx
	cmpq	%rdx, %rsi
	jb	.L12
.L3:
	shrq	$2, %rsi
	movl	%esi, %ebx
.L5:
	movq	$0, (%rax)
	movl	%ebx, (%rax)
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	$40, %edx
	movl	$8, %ebx
	cmpq	%rdx, %rsi
	jnb	.L3
.L12:
	movq	%rdx, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	jne	.L5
	popq	%rbx
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE5:
	.size	vec_from_memview, .-vec_from_memview
	.ident	"GCC: (GNU) 13.2.1 20230801"
	.section	.note.GNU-stack,"",@progbits
