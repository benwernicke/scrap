#include <holy_types.h>
#include <stdlib.h>

typedef struct Vec Vec;
struct Vec {
    u32 cap;
    u32 len;
    int  buffer[];
};

Vec* vec_create(u32 cap) {
    if (cap == 0) cap = 8;
    Vec* v = malloc(sizeof(*v) + cap * sizeof(int));
    if (!v) return nullptr;
    *v = (Vec) {
        .cap = cap,
    };
    return v;
}
