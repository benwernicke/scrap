#include <holy_types.h>
#include <stdlib.h>

typedef struct Vec Vec;
struct Vec {
    u32 cap;
    u32 len;
    int buffer[];
};

Vec* vec_create(void) {
    Vec* v = malloc(sizeof(*v) + 8 * sizeof(int));
    if (!v) return nullptr;
    *v = (Vec) {
        .cap = 8,
    };
    return v;
}
