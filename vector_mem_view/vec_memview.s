	.file	"vec_memview.c"
	.text
	.p2align 4
	.globl	vec_create
	.type	vec_create, @function
vec_create:
.LFB5:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movl	16(%rsp), %ebx
	movq	24(%rsp), %rax
	movq	32(%rsp), %rdx
	testl	%ebx, %ebx
	je	.L7
	movl	%ebx, %ecx
	testq	%rax, %rax
	leaq	8(,%rcx,4), %rsi
	je	.L3
.L15:
	movq	%rdx, %rbx
	shrq	$2, %rbx
	cmpq	%rsi, %rdx
	jb	.L14
.L4:
	movq	$0, (%rax)
	movl	%ebx, (%rax)
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	testq	%rax, %rax
	movl	$40, %esi
	movl	$8, %ebx
	jne	.L15
.L3:
	movq	%rsi, %rdi
	call	malloc@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rax, %rdi
	call	realloc@PLT
.L5:
	testq	%rax, %rax
	jne	.L4
	popq	%rbx
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE5:
	.size	vec_create, .-vec_create
	.ident	"GCC: (GNU) 13.2.1 20230801"
	.section	.note.GNU-stack,"",@progbits
