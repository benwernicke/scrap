#include <stdlib.h>
#include <holy_types.h>

typedef struct Mem_View Mem_View;
struct Mem_View {
    u8* buffer;
    usize cap;
};

typedef struct Vec Vec;
struct Vec {
    u32 cap;
    u32 len;
    int buffer[];
};

typedef struct Ctx Ctx;
struct Ctx {
    u32 cap;
    Mem_View view;
};

Vec* vec_create(Ctx ctx) {
    if (ctx.cap == 0) ctx.cap = 8;

    usize needed_bytes = sizeof(Vec) + sizeof(int) * ctx.cap;
    Vec* v;

    if (ctx.view.buffer) {
        if (ctx.view.cap < needed_bytes) {
            ctx.view.buffer = realloc(ctx.view.buffer, needed_bytes);
        } 
        ctx.cap = ctx.view.cap / sizeof(int);

        v = (void*)ctx.view.buffer;
    } else {
        v = malloc(needed_bytes);
    }
    if (!v) return nullptr;
    *v = (Vec) {
        .cap = ctx.cap,
    };
    return v;
}
