	.file	"vec_ctx.c"
	.text
	.p2align 4
	.globl	vec_create
	.type	vec_create, @function
vec_create:
.LFB5:
	.cfi_startproc
	testl	%edi, %edi
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	je	.L4
	movl	%edi, %eax
	movl	%edi, %ebx
	leaq	8(,%rax,4), %rdi
.L2:
	call	malloc@PLT
	testq	%rax, %rax
	je	.L1
	movq	$0, (%rax)
	movl	%ebx, (%rax)
.L1:
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movl	$40, %edi
	movl	$8, %ebx
	jmp	.L2
	.cfi_endproc
.LFE5:
	.size	vec_create, .-vec_create
	.ident	"GCC: (GNU) 13.2.1 20230801"
	.section	.note.GNU-stack,"",@progbits
