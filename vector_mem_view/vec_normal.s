	.file	"vec_normal.c"
	.text
	.p2align 4
	.globl	vec_create
	.type	vec_create, @function
vec_create:
.LFB5:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$40, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L1
	movq	$8, (%rax)
.L1:
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE5:
	.size	vec_create, .-vec_create
	.ident	"GCC: (GNU) 13.2.1 20230801"
	.section	.note.GNU-stack,"",@progbits
