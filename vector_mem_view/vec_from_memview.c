#include <holy_types.h>
#include <stdlib.h>

typedef struct Vec Vec;
struct Vec {
    u32 cap;
    u32 len;
    int buffer[];
};

typedef struct Mem_View Mem_View;
struct Mem_View {
    void* buffer;
    usize cap;
};

typedef struct Ctx Ctx;
struct Ctx {
    u32 cap;
};

Vec* vec_from_memview(Mem_View view, Ctx ctx) {
    if (ctx.cap == 0) ctx.cap = 8;
    usize bytes_needed = sizeof(Vec) + ctx.cap * sizeof(int);

    if (view.cap < bytes_needed) {
        view.buffer = realloc(view.buffer, bytes_needed);
        if (!view.buffer) return nullptr;
    } else {
        ctx.cap = view.cap / sizeof(int);
    }

    Vec* v = view.buffer;
    *v = (Vec) {
        .cap = ctx.cap,
    };
    return v;
}
