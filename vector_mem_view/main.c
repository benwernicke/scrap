#include <holy_types.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

typedef int Element;

typedef struct Vec Vec;
struct Vec {
    u32 cap;
    u32 len;
    Element buffer[];
};

typedef struct Vec_Create_Ctx Vec_Create_Ctx;
struct Vec_Create_Ctx {
    u32 cap;
};

typedef struct Mem_View Mem_View;
struct Mem_View {
    u8* buffer;
    usize cap;
};

Vec* vec_create_(Vec_Create_Ctx ctx) {
    if (ctx.cap == 0) {
        ctx.cap = 8;
    } 
    Vec* v = malloc(sizeof(*v) + ctx.cap * sizeof(*v->buffer));
    if (!v) return nullptr;
    *v = (Vec) {
        .cap = ctx.cap,
    };
    return v;
}

#define vec_create(...) vec_create_((Vec_Create_Ctx){__VA_ARGS__})

Vec* vec_from_mem_view_(Mem_View view, Vec_Create_Ctx ctx) {
    if (ctx.cap == 0) {
        ctx.cap = 8;
    }
    if (view.cap < sizeof(Vec) + sizeof(Element) * ctx.cap) {
        view.buffer = realloc(view.buffer, sizeof(Vec) + sizeof(Element) * ctx.cap); 
        if (!view.buffer) return nullptr;
    }
    Vec* v = (void*)view.buffer;
    *v = (Vec) {
        .cap = ctx.cap,
    };
    return v;
}

#define vec_from_mem_view(view, ...) vec_from_mem_view_(view, (Vec_Create_Ctx) { __VA_ARGS__ })

Mem_View vec_to_mem_view(Vec v[static 1]) {
    return (Mem_View) {
        .cap = sizeof(*v) + sizeof(Element) * v->cap,
        .buffer = (void*)v,
    };
}

void vec_free(Vec* v) {
    if (!v) return;
    free(v);
}

int main(void) {
    auto v0 = vec_create();
    auto v1 = vec_from_mem_view(vec_to_mem_view(v0));
    return 0;
}
