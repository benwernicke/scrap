#include <stdlib.h>
#include <holy_types.h>

typedef struct Vec Vec;
struct Vec {
    u32 cap;
    u32 len;
    int buffer[];
};

typedef struct Ctx Ctx;
struct Ctx {
    u32 cap;
};

Vec* vec_create(Ctx ctx) {
    if (ctx.cap == 0) ctx.cap = 8;
    Vec* v = malloc(sizeof(*v) + sizeof(int) * ctx.cap);
    if (!v) return nullptr;
    *v = (Vec) {
        .cap = ctx.cap,
    };
    return v;
}
