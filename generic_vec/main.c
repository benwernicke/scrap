#include <stdio.h>
#include <stdlib.h>
#include <holy_types.h>


/*---------------------------------------------------------*/

typedef struct Vec_U64 Vec_U64;
struct Vec_U64 {
    u64* buf;
    usize size;
    usize cap;
};

int vec_u64_init(Vec_U64 v[static 1]) {
    *v = (Vec_U64) {
        .cap = 16,
    };
    v->buf = malloc(sizeof(u64[v->cap]));
    return v->buf ? 0 : -1;
}

void vec_u64_deinit(Vec_U64 v[static 1]) {
    free(v->buf);
}

Vec_U64 vec_u64_create(void) {
    return (Vec_U64) { };
}

u64* vec_u64_begin(Vec_U64 v[static 1]) {
    return v->buf;
}

u64* vec_u64_end(Vec_U64 v[static 1]) {
    return v->buf + v->size;
}

u64* vec_u64_more(Vec_U64 v[static 1]) {
    if (v->size >= v->cap) {
        v->cap <<= 1;
        v->buf = realloc(v->buf, sizeof(u64[v->cap]));
    }
    return &v->buf[v->size++];
}

/*---------------------------------------------------------*/

typedef struct Vec_Float Vec_Float;
struct Vec_Float {
    float* buf;
    usize size;
    usize cap;
};

int vec_float_init(Vec_Float v[static 1]) {
    *v = (Vec_Float) {
        .cap = 16,
    };
    v->buf = malloc(sizeof(float[v->cap]));
    return v->buf ? 0 : -1;
}

void vec_float_deinit(Vec_Float v[static 1]) {
    free(v->buf);
}

float* vec_float_begin(Vec_Float v[static 1]) {
    return v->buf;
}

float* vec_float_end(Vec_Float v[static 1]) {
    return v->buf + v->size;
}

float* vec_float_more(Vec_Float v[static 1]) {
    if (v->size >= v->cap) {
        v->cap <<= 1;
        v->buf = realloc(v->buf, sizeof(float[v->cap]));
        if (!v->buf) return NULL;
    }
    return &v->buf[v->size++];
}

Vec_Float vec_float_create(void) {
    return (Vec_Float) { };
}

#define PANIC() exit(1)

#define make_vec_(t) _Generic((t), \
            u64 : (Vec_U64) { }, \
            float : (Vec_Float) { }, \
            default :  PANIC())

#define make_vec(t) make_vec_((t){})

#define vec_more(v) _Generic((v), \
        Vec_Float* : vec_float_more((Vec_Float*)v), \
        Vec_U64* : vec_u64_more((Vec_U64*)v))

#define vec_deinit(v) _Generic((v), \
        Vec_Float* : vec_float_deinit((Vec_Float*)v), \
        Vec_U64* : vec_u64_deinit((Vec_U64*)v))

#define vec_init(t) _Generic((t), \
        Vec_Float* : vec_float_init((Vec_Float*)t), \
        Vec_U64* : vec_u64_init((Vec_U64*)t))

#define vec_begin(t) _Generic((t),\
        Vec_U64* : vec_u64_begin((Vec_U64*)t), \
        Vec_Float* : vec_float_begin((Vec_Float*)t))

#define vec_end(t) _Generic((t), \
        Vec_U64* : vec_u64_end((Vec_U64*)t), \
        Vec_Float* : vec_float_end((Vec_Float*)t))

int main(void) {

    auto vu64 = &make_vec(u64);
    auto vfloat = &make_vec(float);

    vec_init(vu64);
    vec_init(vfloat);

    for (usize i = 0; i < 100; ++i) {
        *vec_more(vu64) = i;
        *vec_more(vfloat) = (float)i / 2;
    }

    {
        printf("u64:\n");
        auto iter = vec_begin(vu64);
        auto end = vec_end(vu64);
        for (; iter != end; ++iter) {
            printf("%lu ", *iter);
        }
        putc('\n', stdout);
    }

    {
        printf("float:\n");
        auto iter = vec_begin(vfloat);
        auto end = vec_end(vfloat);
        for (; iter != end; ++iter) {
            printf("%f ", *iter);
        }
        putc('\n', stdout);
    }

    vec_deinit(vu64);
    vec_deinit(vfloat);

    return 0;
}
