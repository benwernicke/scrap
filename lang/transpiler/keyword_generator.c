#include <stdio.h>
#include <string.h>
#include <holy_types.h>
#include <stdlib.h>

#include "panic.h"

int main(int argc, char** argv) {
    ++argv;
    while (*argv) {
        usize len = strlen(*argv);
        PANIC_IF(len > 8, "keyword is too long consider different");
        u64 word = 0;
        memcpy(&word, *argv, len);
        printf("#define KEYWORD_LITERAL_%s 0x%lxlu\n", *argv, word);
        ++argv;
    }

    return 0;
}
