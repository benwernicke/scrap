# Design

## Tokenization
- [ ] Use static perfect hashmaps for language keywords
- [ ] use file as buffer for all user defined names
  - [ ] use set of String_Views to check
- [ ] tokens should be seperated into different blocks
  - [ ] first iteration of tokens can just be pointers to String_Views in the
        set
