#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h> // TODO: needs replacement
#include <holy_types.h>
#include <ctype.h>
#include <stdbool.h>

#include "panic.h"

#define TODO() \
    do { \
        fprintf(stderr, "%s:%d is not implemented\n", __func__, __LINE__); \
    } while (0)

// ================ String_View ================
typedef struct String_View String_View;
struct String_View {
    const char* str;
    u16       len;
};

// ============== Token ================
typedef enum {
    TOK_NAME,

    // symbols
    TOK_SEMICOLON,
    TOK_PLUS,
    TOK_STAR,
    TOK_MINUS,

    // key words
    TOK_KW_FOR,
    TOK_KW_IF,
    TOK_KW_WHIL,
    TOK_KW_ELSE,
    TOK_KW_DO,
    TOK_KW_ELIF,

} Token;

#define KEYWORD_LITERAL_FOR   0x726f66lu
#define KEYWORD_LITERAL_IF    0x6669lu
#define KEYWORD_LITERAL_WHILE 0x656c696877lu
#define KEYWORD_LITERAL_ELSE  0x65736c65lu
#define KEYWORD_LITERAL_DO    0x6f64lu
#define KEYWORD_LITERAL_ELIF  0x66696c65lu

Token get_keyword(String_View* word) {
    if (word->len > 8) return TOK_NAME;

    u64 as_number = 0;
    memcpy(&as_number, word->str, word->len);

    switch (as_number) {
        case KEYWORD_LITERAL_FOR:   return TOK_KW_FOR;
        case KEYWORD_LITERAL_IF:    return TOK_KW_IF;
        case KEYWORD_LITERAL_WHILE: return TOK_KW_WHILE;
        case KEYWORD_LITERAL_ELSE:  return TOK_KW_ELSE;
        case KEYWORD_LITERAL_DO:    return TOK_KW_DO;
        case KEYWORD_LITERAL_ELIF:  return TOK_KW_ELIF;
        default:
            return TOK_NAME;
    }
}

int main(int argc, char** argv) {
    PANIC_IF(argc != 2, "wrong number of arguments: expected 2 got %d\n", argc);

    printf("Hello World!\n");

    // load file to name buffer and set cursor
    char* buffer = NULL;
    char* write_cursor = NULL;
    const char* read_cursor = NULL;

        FILE* f = fopen(argv[1], "r");
        PANIC_IF(!f, "could not open file: '%s': %s\n", argv[1], strerror(errno));

        // get len of file
        fseek(f, 0, SEEK_END);
        usize len = ftell(f);
        rewind(f);

        // allocate buffer with a bit of space to seperate reading and writing
        buffer = malloc(len + 128);
        PANIC_IF(!buffer, "out of mem for file: %s: %s", argv[1], strerror(errno));

        // read file into buffer
        usize read_bytes = fread(buffer + 128, 1, len, f);
        PANIC_IF(read_bytes != len, "could not read entire file: '%s': %s\n", argv[1], strerror(errno));

        fclose(f);

        // set buffer pointer for reading and writing
        write_cursor = buffer;
        read_cursor = buffer + 128;
        const char* const buffer_end = buffer + len + 128;


    // tokenization looop
    while (read_cursor != buffer_end) {
        // skip whitespace
        for (; isspace(read_cursor) && read_cursor != buffer_end; ++read_cursor) { }

        char c = *read_cursor;
        if (isalpha(c)) {
            const char* begin = read_cursor;

            for (bool in_token = 1; in_token; ++read_cursor) {
                in_token |= isalpha(*read_cursor);
                in_token |= *read_cursor == '_';
                in_token |= isdigit(*read_cursor);
            }

            Token t = get_keyword((String_View) { .str = begin, .len = read_cursor - begin });

            TODO();
            // TODO: lookup name
            // TODO: if not known add name to buffer
            // generate token
            
        } else if (isdigit(c)) {
            const char* begin = read_cursor;

            // TODO read hex, oct, and bin
            // TODO read floats
            for (bool in_token = 1; in_token; ++read_cursor) {
                in_token |= isdigit(*read_cursor);     
            }
            
            TODO();
            // TODO: token to number
            // how to store number literals?

        } else {
            read_cursor += 1;
            switch (c) {
                case '+': {
                    TODO();
                } 
                break;
                case '-': {
                    TODO();
                } 
                break;
                case '*': {
                    TODO();
                } 
                break;
                default:
                    PANIC("encoutered unknown character '%c'\n", c);
            }
        }
    }

    free(buffer);
    return 0;
}
