#ifndef PANIC_H
#define PANIC_H

#include <stdarg.h>
#include <stdio.h>

#define PANIC(...) \
    do { \
        fprintf(stderr, "Panic: "); \
        fprintf(stderr, __VA_ARGS__); \
        fprintf(stderr, "\n"); \
        exit(1); \
    } while (0)


#define PANIC_IF(b, ...) \
    do { \
        if (b) { \
            PANIC(__VA_ARGS__); \
        } \
    } while (0)

#endif // PANIC_H
