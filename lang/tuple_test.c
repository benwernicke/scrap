#include <stdio.h>
#include <holy_types.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdarg.h>

#define Result(f) Result_##f

struct { u32 value; u32 rest; bool is_undefined; } 
divide(u32 a, u32 b) {
    if (b == 0) return (typeof(divide(0, 0))) { 0, 0, 1};
    return (typeof(divide(0, 0))) { a / b, a % b, 0 };
}

struct { u32 value; bool is_undefined; }
cstr_to_u32(char* s) {
    u32 sum = 0;
    for (; *s; ++s) {
        if (!isdigit(*s)) return (typeof(cstr_to_u32(""))) { .is_undefined = 1 };
        sum *= 10;
        sum += *s - '0';
    }
    return (typeof(cstr_to_u32(""))) { .value = sum };
}

#define result_unpack(FUNC, DEFINED, UNDEFINED) \
    do { \
        typeof(FUNC) result = FUNC; \
        if (result.is_undefined) { \
            UNDEFINED; \
        } else { \
            DEFINED; \
        } \
    } while (0)

#define PANIC(...) \
    do { \
        fprintf(stderr, __VA_ARGS__); \
        exit(1); \
    } while (0)

int main(int argc, char** argv) {
    u32 a;
    result_unpack(cstr_to_u32(argv[1]),
            { a = result.value; },
            { PANIC("Error: '%s' is not an u32\n", argv[1]); });

    u32 b;
    result_unpack(cstr_to_u32(argv[2]),
            { b = result.value; },
            { PANIC("Error: '%s' is not an u32\n", argv[2]); });

    result_unpack(divide(a, b),
            { 
                a = result.value;
                b = result.rest;
            },
            { PANIC("Error: operation: %u / %u is undefined\n", a, b); });

    printf("%u, %u\n", a, b);

    return 0;
}
