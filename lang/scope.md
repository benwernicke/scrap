# Some new Programming Language
We all need more programming languages

## General Guidelines
- KISS
- simple to read
- simple to write
- no hidden control flow
- compilation should be very fast
- systems programming language
- some form of generic programming should be possible
  - Result Type for functions that can fail
  - Future Type for ansync
- should be easy to write parallel code
- std library should not be necessary to do anything
  - compiler options that generate entry point code
  - simple non stdlib dependent
- compiler should include dependency manager and build tool
- easy interop with other languages
- no unnecessary compiler flags, everything errors

## Names
- ether
- zest
- alil - all languages i liked
- bopl - bens obvious programming language
- tune

## Features from other languages I want
- inline asm
- Zig comptime
- multiple return types
- go routines
- undefined as rvalue;
- defer
- haskell decls with type attributes

## Questions
- static vs dynamic linking

## Syntax
- lowercase for primitive types
- Camel_Case for higher types
- snake_case for variables and functions
- $*$ should indicate pointer

### Keywords
- if
- else
- elif
- match
- while
- for
- do { } while ;

### Functions
C Style Functions:
```c
i32 add(i32 a, i32 b) {
    return a + b;
}
```
Haskell Style Functions
```haskell
add:: i32 -> i32 -> i32
add = a -> b -> a + b
```
Go Functions / Modern Function in general
```go
[public]
fn add(a: i32, b: i32) i32 {
    return a + b;
}
```

## Scopes
Scopes should be supported
```c
Scope std = include(std);
std::print("Hello World\n");

// or

Scope std::io = import(std::io);
```

## Type System
- use holy types as primitives
- strong type system
- type decorator / attribute to indicate nonnull, pure, const, thread_safe etc.
- Arrays have length associated with them
> _Note:_ is there a better way?

## Public Private
- private is default
- public could be set with type decorators
